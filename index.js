const express = require('express')
const app = express()
const path = require('path')
const PORT = process.env.PORT || 3000


app.use(express.static(path.join(__dirname, 'public')))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
app.get('/', (req, res) => res.render('pages/index', {
  page: 'home'
}));
app.get('/license', (req, res) => res.render('pages/index', {
  page: 'license'
}));
app.get('/contact', (req, res) => res.render('pages/index', {
  page: 'contact'
}));
app.get('/contamination', (req, res) => res.render('pages/index', {
  page: 'contamination'
}));
app.listen(PORT, () => console.log(`Listening on ${ PORT }`))
